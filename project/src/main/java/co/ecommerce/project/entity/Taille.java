package co.ecommerce.project.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.GenerationType;


@Entity
public class Taille {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer taille;
    private Integer stock;
    @ManyToOne
    private LignePanier panier;
    @ManyToOne
    private Produit produit;

    public Taille(Integer taille, Integer stock, LignePanier panier, Produit produit) {
        this.taille = taille;
        this.stock = stock;
        this.panier = panier;
        this.produit = produit;
    }
    public Taille(Integer id, Integer taille, Integer stock, LignePanier panier, Produit produit) {
        this.id = id;
        this.taille = taille;
        this.stock = stock;
        this.panier = panier;
        this.produit = produit;
    }
    public Taille() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getTaille() {
        return taille;
    }
    public void setTaille(Integer taille) {
        this.taille = taille;
    }
    public Integer getStock() {
        return stock;
    }
    public void setStock(Integer stock) {
        this.stock = stock;
    }
    public LignePanier getPanier() {
        return panier;
    }
    public void setPanier(LignePanier panier) {
        this.panier = panier;
    }
    public Produit getProduit() {
        return produit;
    }
    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}

