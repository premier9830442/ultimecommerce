package co.ecommerce.project.controller;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Note;
import co.ecommerce.project.entity.User;
import co.ecommerce.project.repository.NoteRepository;

@RequestMapping("api/note")
@RestController
public class NoteController {

    @Autowired
    NoteRepository repo;

    @GetMapping
    public List<Note> allNotes(){
        return repo.findAll();
    }
    @GetMapping("/{id}")
    public Note oneNote(@PathVariable int id){
        return repo.findById(id).get();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Note  addNote(@RequestBody Note note, @AuthenticationPrincipal User user){
        note.setAuthor(user);
        note.setDate(LocalDateTime.now());
        return repo.save(note);
    }
    @PutMapping("/{id}")
    public Note updateNote(@PathVariable int id, @RequestBody Note note, @AuthenticationPrincipal User user){
        Note notetoUpdate = oneNote(id);
        if(!user.getRole().equals("ROLE_ADMIN") && user.getRole().equals("ROLE_SUPER_ADMIN") && !note.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        notetoUpdate.setContent(note.getContent());
        repo.save(notetoUpdate);
        return notetoUpdate;

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user){
        Note note = oneNote(id);
        if(!user.getRole().equals("ROLE_ADMIN") && user.getRole().equals("ROLE_SUPER_ADMIN") && !note.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        repo.delete(note);
    }


}

