package co.ecommerce.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Categorie;
import co.ecommerce.project.repository.CategorieRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/categorie")

public class CategorieController {

    @Autowired
    private CategorieRepository repo;

    @GetMapping
    public List<Categorie> all() {
        return repo.findAll();
    }
    
    @GetMapping("/{id}")
    public Categorie one (@PathVariable int id) {
        return repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie add (@Valid @RequestBody Categorie categorie) {
        repo.save(categorie);
        return categorie;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable int id) {
        Categorie cat = one(id);
        repo.delete(cat);
    }
    
        @PutMapping("/{id}")
        public Categorie update (@PathVariable int id, @Valid @RequestBody Categorie categorie) {
            Categorie toUpdate = one(id);
            toUpdate.setName(categorie.getName());
            repo.save(toUpdate);
            return toUpdate; 
        }





}
