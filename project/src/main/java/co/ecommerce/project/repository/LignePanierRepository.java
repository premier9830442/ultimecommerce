package co.ecommerce.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.LignePanier;

@Repository
public interface LignePanierRepository extends JpaRepository<LignePanier, Integer> { 
}
